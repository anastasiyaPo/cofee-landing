import Link from "next/link"
import styles from './styled.module.scss'
import {MENU} from "@/components/header/constants";
import {Trans, useTranslation} from "next-i18next";
import {useState} from "react";
import SvgMenu from '../../../public/images/icons/menu.svg';

export default function Header() {
  const { t } = useTranslation("header");

  const [openMenu, setOpenMenu] = useState(false);

  return (
    <header className={styles._core}>
      <div className={styles.container}>
        <Link href={"/"} className={styles.title}>
          <Trans
            t={t}
            i18nKey="title"
            components={{
              span: <span />,
            }}
          />
        </Link>

        <nav className={openMenu ? styles.navigate_open : undefined}>
          <button
            type="button"
            className={styles.targetMenuButton}
            tabIndex={-1}
            onClick={() => setOpenMenu(p => !p)}
            hidden
            title={t(`targetButton.${!openMenu}`) as string}
          >
            {t(`targetButton.${!openMenu}`)}
            <SvgMenu heigth={16} />
          </button>
          <ul className={styles.navigationList}>
            {MENU.map(it =>
              <li key={it.id}>
                <Link
                  href={it.href}
                  onClick={() => setOpenMenu(false)}
                >{t(`menu.${it.id}`)}</Link>
              </li>
            )}
          </ul>
        </nav>
      </div>
    </header>
  )
}
