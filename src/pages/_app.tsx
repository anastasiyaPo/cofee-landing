import '@/styles/globals.css'
import type { AppProps } from 'next/app'
import { appWithTranslation } from "next-i18next"
import Header from "@/components/header";

function App({ Component, pageProps }: AppProps) {
  return (
    <div>
      <Header />
      <Component {...pageProps} />
    </div>
  )
}

export default appWithTranslation(App);
