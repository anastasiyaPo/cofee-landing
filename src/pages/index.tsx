import Head from 'next/head'
import styles from '@/styles/Home.module.scss'
import { useTranslation } from "next-i18next";
import {GetStaticProps} from "next";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {DEFAULT_LANGUAGES} from "@/constants/lacales";

export const getStaticProps: GetStaticProps = async ({ locale }) => {
  const localeProps = await serverSideTranslations(
    locale ?? DEFAULT_LANGUAGES,
    [
      "common",
      "header"
    ],
  );

  return { props: { ...localeProps } };
};


export default function Home() {
  const { t } = useTranslation();

  return (
    <>
      <Head>
        <title>Coffee</title>
        <meta name="description" content="Example of landing. Theme: coffee" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.png" />
      </Head>
      <div>
        <main className={styles.main}>
          <p>{t("test")}</p>
        </main>
      </div>
    </>
  )
}
